﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Excel = Microsoft.Office.Interop.Excel;
namespace lab2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Random x = new Random();
        Random y = new Random();
        public System.Windows.Point p = new System.Windows.Point();
        public System.Windows.Point[] pi = new System.Windows.Point[2];
        int Status_exp = 0;
        DateTime Start;
        DateTime Stop;
        TimeSpan Elapsed = new TimeSpan();
        public System.Windows.Point s;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void ButtonStart_click(object sender, RoutedEventArgs e)
        {
            lbRez.Items.Clear();
            lbRez.Items.Add("Результати");
            expir();
        }
        private System.Windows.Point DrowObject(System.Windows.Point p)
        {
            System.Windows.Shapes.Ellipse el = new System.Windows.Shapes.Ellipse();
            el.Width = 15;
            el.Height = 15;
            p.X = x.Next(Convert.ToInt32(this.Width - el.Width));
            p.Y = y.Next(Convert.ToInt32(icDrow.ActualHeight - el.Height));
            el.Fill = Brushes.Gray;
            InkCanvas.SetLeft(el, p.X);
            InkCanvas.SetTop(el, p.Y);
            icDrow.Children.Add(el);
            return p;
        }
        public void expir()
        {
            int[] rez_arr = new int[2];
            Array.Clear(pi, 0, pi.Length - 1);
            Start = new DateTime(0);
            icDrow.Children.Clear();
            icDrow.Strokes.Clear();
            for (int i = 0; i < 2; i++)
            {
                pi[i] = DrowObject(p);
            }
            Status_exp = Status_exp + 1;
        }
        object misValue = System.Reflection.Missing.Value;
        private void icUp(object sender, MouseButtonEventArgs e)
        {
            if (Status_exp > 0)
            {
                System.Windows.Point[] n = Array.FindAll(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20));
                if (Array.Exists(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20)))
                {
                    if (!((Math.Abs(s.X - e.GetPosition(this).X) < 10) && (Math.Abs(s.Y - e.GetPosition(this).Y) < 10)))
                    {
                        s = e.GetPosition(this);
                        {
                            
                            if (Start.Ticks == 0) { Start = DateTime.Now; }
                            else
                            {
                                Stop = DateTime.Now;
                                Elapsed = Stop.Subtract(Start);
                                long elapsedTicks = Stop.Ticks - Start.Ticks;
                                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                                double rez = Math.Sqrt(Math.Pow(pi[0].X - pi[1].X, 2) + Math.Pow(pi[0].Y - pi[1].Y, 2));
                                lbRez.Items.Add("Експеремент: " + Status_exp.ToString() + " " + "Час: " + elapsedSpan.Milliseconds.ToString() + " " + " Відстань: " + rez);
                                if (Status_exp < 100) expir();
                                Excel.Application ex = new Excel.Application();
                                Excel.Workbook workBook = ex.Workbooks.Add(Type.Missing);
                                Excel.Worksheet sheet = (Excel.Worksheet)ex.Worksheets.get_Item(1);
                                sheet.Name = "lab2";
                                sheet.Cells[Status_exp + 1, 1] = String.Format("{0}", Status_exp.ToString());
                                sheet.Cells[Status_exp + 1, 2] = String.Format("{0}", elapsedSpan.Milliseconds.ToString());
                                sheet.Cells[Status_exp + 1, 3] = String.Format("{0}", rez.ToString());
                                ex.Application.ActiveWorkbook.SaveAs(@"C:\Users\Артем\Desktop\ЛМІ\lab2\lab2.xlsx");
                                ex.Quit();

                            }
                        }
                    }
                }

            }
        }
    }
}
